# node-red-contrib-digit-http10-server

This module adds a new Palette in Node-RED that offers legacy HTTP/1.0 functionality. The Nodes will be groupped under the `digit` category.

It contains 3 nodes: 
 - http10-in
 - http10-out
 - http10-request


## http10-in

An HTTP/1.0 server.


Configuration elements:
 - Name
 - Port


## http10-out

An HTTP/1.0 server response.


Configuration elements:
 - Name
 - StatusCode


## http10-request

An HTTP/1.0 client.


Configuration elements:
 - Name
 - Method
 - Host
 - Port
 - Path

module.exports = function(RED) {
    function http10outNode(config) {
      RED.nodes.createNode(this, config);
      var node = this;

      var code = config.code * 1;

      node.on('input', function(msg) {

        let message = "";
        // handle more codes eventually
        if (code == 400) message = "Bad Request";
        else message = "OK";

        const response = `HTTP/1.0 ${code} ${message}\r\nContent-Type: text/html\r\nConnection: close\r\n\r\n`
        msg.writeCallback(response);
        msg.writeCallback(msg.payload);
        msg.endCallback();

		}); 

		}
		RED.nodes.registerType("http10-out", http10outNode);
	}

module.exports = function(RED) {
    function http10in(config) {
      RED.nodes.createNode(this, config);
	    var node = this;
	
      var net = require("net");

	    var port = config.port * 1;


		  server = net.createServer(function(socket) {

        socket.on('data', (data) => {

          const request = data.toString();

          if (request.startsWith('GET')) {
            const writeCallback = socket.write.bind(socket);
            const endCallback = socket.end.bind(socket);

            let requestHeaders = {};
            const [headerString, body] = request.split('\r\n\r\n');

            // Parse response headers
            const headersArray = headerString.split('\r\n');
            for (const header of headersArray.slice(1)) {
              const [key, value] = header.split(': ');
              requestHeaders[key.toLowerCase()] = value;
            }


            var msg = {}
            msg.writeCallback = writeCallback;
            msg.endCallback = endCallback;
            msg.socket = socket;
            msg.payload = ""; //response;
            msg.requestHeaders = requestHeaders;
            node.send(msg, false);
          }

		    });

        socket.on('error', (error) => {
          node.warn("Socket error: ", error);
        });
      });

		  server.listen(port, '0.0.0.0', () => {
        // server is running
      });
		  
		  node.on('close', function() {
		  		node.closing = true;
		  		server.close();
		  		node.log(RED._("http10 stopped"));
		  	});

		  }
		  RED.nodes.registerType("http10-in", http10in);
	}

module.exports = function(RED) {
    function http10requestNode(config) {
        RED.nodes.createNode(this, config);
		    var node = this;
	
        var net = require("net");

	    	var port = config.port * 1;
	    	var host = config.host + '';
	    	var path = config.path + '';
	    	var method = config.method + '';

	    	node.on('input', function(msg) {

          if (msg.hasOwnProperty('host')) host = msg.host;

          // Define the HTTP request with the specified method
          const httpRequest = `${method} ${path} HTTP/1.0\r\nHost: ${host}\r\nConnection: close\r\n\r\n`;

          // Create a TCP socket connection to the server
          const client = net.createConnection({ port: port, host: host }, () => {
            // Send the HTTP request to the server
            client.write(httpRequest);
          });

          // Variables to store response data and headers
          let responseData = '';
          let responseHeaders = {};

          // Handle incoming data from the server
          client.on('data', (data) => {
            // Log the response from the server
            responseData += data.toString();
          });

          // Handle connection closure
          client.on('end', () => {
            // Split response into headers and body
            const [headerString, body] = responseData.split('\r\n\r\n');

            // Parse response headers
            const headersArray = headerString.split('\r\n');
            for (const header of headersArray.slice(1)) {
              const [key, value] = header.split(': ');
              responseHeaders[key.toLowerCase()] = value;
            }

            // Extract status code from first line of headers
            const [statusLine] = headersArray;
            const [, statusCode] = statusLine.split(' ');

            msg.payload = body;
            msg.responseHeaders = responseHeaders;
            msg.statusCode = parseInt(statusCode);

            // Send message object
            node.send(msg);
          });

          // Handle connection errors
          client.on('error', (err) => {
            node.error('Connection error:', err);
          });
	    	});


		}
		RED.nodes.registerType("http10-request", http10requestNode);
	}
